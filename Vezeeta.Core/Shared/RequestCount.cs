﻿using Vezeeta.Core.Enums;

namespace Vezeeta.Core.Shared
{
    public class RequestCount
    {
        public string Status { get; set; }
        public int Count { get; set; }
    }
}
